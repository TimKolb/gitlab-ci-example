import React, { Component } from "react";

export class ApiTest extends Component {
  state = {
    loading: false,
    data: null
  };

  async componentDidMount() {
    this.setState({ loading: true });
    const response = await fetch("/api/test");
    const data = await response.text();
    this.setState({ data, loading: false });
  }

  render() {
    const { loading, data } = this.state;

    if (loading) {
      return <p>Loading...</p>;
    }

    if (data) {
      return <p>{data}</p>;
    }

    return null;
  }
}
