#!/usr/bin/env sh

set -ex

docker pull $TAGGED_IMAGE_TAG_API || true
docker pull $IMAGE_TAG_API:develop || true
docker pull $TAGGED_IMAGE_TAG_DATABASE || true
docker pull $IMAGE_TAG_DATABASE:develop || true

docker build \
    --cache-from $TAGGED_IMAGE_TAG_API \
    --cache-from $IMAGE_TAG_API:develop \
    -t $TAGGED_IMAGE_TAG_API \
    -f .docker/api/Dockerfile \
    api

docker build \
    --cache-from $TAGGED_IMAGE_TAG_DATABASE \
    --cache-from $IMAGE_TAG_DATABASE:develop \
    -t $TAGGED_IMAGE_TAG_DATABASE \
    -f .docker/database/Dockerfile \
    api

docker push $TAGGED_IMAGE_TAG_API
docker push $TAGGED_IMAGE_TAG_DATABASE
