const express = require('express');

const app = express();

app.get('*', (req, res) => res.send('Hello World!'));

const server = app.listen(process.env.PORT || 80, () => {
    const address = server.address();
    const {address: host, port} = address;
    console.log('🚀 Listening on %s:%s', host, port);
});
